<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Admin\Controller;

use Application\Video\Model\Video;
use Application\Video\Model\VideoTable;

use Application\Collection\Model\Collection;
use Application\Collection\Model\CollectionTable;

use Application\Event\Model\Event;
use Application\Event\Model\EventTable;

use Application\User\Model\User;
use Application\User\Model\UserTable;
use Application\User\Form\UserForm;


use Application\Poll\Model\Poll;
use Application\Poll\Model\PollTable;
use Application\Poll\Model\PollOption;
use Application\Poll\Model\PollOptionTable;


use Application\Collection\Form\CollectionForm;       // <-- Add this import
use Application\Video\Form\VideoForm;       // <-- Add this import
use Application\Event\Form\EventForm;       // <-- Add this import
use Application\Poll\Form\PollForm;       // <-- Add this import
use Application\Poll\Form\PollOptionForm;       // <-- Add this import


use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class AdminController extends AbstractActionController
{
	protected $videoTable;
	protected $collectionTable;
	protected $eventTable;
	protected $pollTable;
	protected $polloptionTable;
	protected $userTable;

	private $authService;
	private $cache;


	public function getAuthService()
	{
		if (!$this->authService)
		{
			$this->authService = $this->getServiceLocator()->get('AuthService');
		}
		return $this->authService;
	}

	public function getCacheService()
	{
		if (!$this->cache)
		{
			$this->cache = $this->getServiceLocator()->get('cache');
		}
		return $this->cache;
	}


	public function onDispatch( \Zend\Mvc\MvcEvent $e )
	{
		if (!$this->getAuthService()->hasIdentity())
		{
		        $routeMatch = $e->getRouteMatch();
			$routeMatch->setParam('action', 'login');
		}

		$this->layout()->setVariable('link', $this->params('action'));
		return parent::onDispatch( $e );
	}

	public function getVideoTable()
	{
		if (!$this->videoTable) {
			$sm = $this->getServiceLocator();
			$this->videoTable = $sm->get('VideoTable');
		}
		return $this->videoTable;
	}

	public function getEventTable()
	{
		if (!$this->eventTable) {
			$sm = $this->getServiceLocator();
			$this->eventTable = $sm->get('EventTable');
		}
		return $this->eventTable;
	}

	public function getPollTable()
	{
		if (!$this->pollTable) {
			$sm = $this->getServiceLocator();
			$this->pollTable = $sm->get('PollTable');
		}
		return $this->pollTable;
	}

	public function getPollOptionTable()
	{
		if (!$this->polloptionTable) {
			$sm = $this->getServiceLocator();
			$this->polloptionTable = $sm->get('PollOptionTable');
		}
		return $this->polloptionTable;
	}

	public function getCollectionTable()
	{
		if (!$this->collectionTable) {
			$sm = $this->getServiceLocator();
			$this->collectionTable = $sm->get('CollectionTable');
		}
		return $this->collectionTable;
	}

	public function getUserTable()
	{
		if (!$this->userTable) {
			$sm = $this->getServiceLocator();
			$this->userTable = $sm->get('UserTable');
		}
		return $this->userTable;
	}


	public function loginAction()
	{
		$request = $this->getRequest();
		if ($request->isPost()) {
			$this->getAuthService()->getAdapter()
				->setIdentity($request->getPost('username'))
				->setCredential($request->getPost('password'));
			$result = $this->getAuthService()->authenticate();
			foreach ($result->getMessages() as $message)
			{
				$this->flashMessenger()->addMessage($message);
			}
			if ($result->isValid())
			{
				$redirect = 'admin';
			}
			return $this->redirect()->toRoute('admin');
		}
		$viewModel = new ViewModel(array('messages' => $this->flashMessenger()->getMessages()));
		$viewModel->setTerminal(true); // Layout won't be rendered
		return $viewModel;		
	}

	public function indexAction()
	{
		$this->layout()->setVariable('area', 'none');
		return new ViewModel();
	}

	public function listcollectionsAction()
	{
		$this->layout()->setVariable('area', 'collections');

		return new ViewModel(array('collections' => $this->getCollectionTable()->fetchAllWithoutDepth()));
	}

	public function deletecollectionAction()
	{
		$collectionid = $this->params()->fromRoute('id', false);   // From POST
		$this->getCollectionTable()->deleteCollection($collectionid);

		$this->getCacheService()->removeItem('collections');
		$this->getCacheService()->addItem('collections', $this->getCollectionTable()->fetchAll());

		return $this->redirect()->toRoute('admin', array('action' => 'listcollections'));
	}

	public function managecollectionAction()
	{
		$this->layout()->setVariable('area', 'none');

		$collectionid = $this->params()->fromRoute('id', false);   // From POST
		$form = new CollectionForm( $this->getCollectionTable()->fetchAllWithoutDepth() , $collectionid);

		$request = $this->getRequest();
		if ($request->isPost()) {
			$collection = new Collection();
			$form->setInputFilter($form->getInputFilter());
			$form->setData($request->getPost());
			if ($form->isValid()) {
				$collection->exchangeArray($form->getData());
				$this->getCollectionTable()->saveCollection($collection);

				$this->getCacheService()->removeItem('collections');
				$this->getCacheService()->addItem('collections', $this->getCollectionTable()->fetchAllArray());

				// Redirect to list of albums
				return $this->redirect()->toRoute('admin', array('action' => 'listcollections'));
			}
		} else {
			if ($collectionid) $form->bind($this->getCollectionTable()->getCollection($collectionid));
		}
		return array('form' => $form);
	}
	
	private function updateVideoCollectionCache($baseID = -1)
	{
		$childids = $this->getCollectionTable()->fetchChildrenIds($baseID);
		if ($baseID == -1)
		{
			foreach ($childids as $id)
			{
				$this->updateVideoCollectionCache($id);
			}
			$this->updateSpecialCollections();
			return;
		} else {
			$childids[] = $baseID;
			$cacheId = 'collection_data_' . $baseID;
			$metaCacheId = 'collection_meta_' . $baseID;
		
			$videoCache = $this->getVideoTable()->getVideosForCollection($childids);
			$cache = array();
			$collection = $this->getCollectionTable()->getCollection($baseID);
			$url  = $collection->url;
			foreach ($videoCache as $row)
			{
				$cache[] = $row;
			}
			$this->getCacheService()->removeItem($cacheId);
			$this->getCacheService()->addItem($cacheId, $cache);
			$this->getCacheService()->removeItem($metaCacheId);
			$this->getCacheService()->addItem($metaCacheId, $collection);
		}
	}
	
	private function updateSpecialCollections()
	{
		$featuredVideos = $this->getVideoTable()->getVideosForCollection(5, true);
		$cache = array();
		foreach ($featuredVideos as $row)
		{
			$cache[] = $row;
		}
		$this->getCacheService()->removeItem('special_collection_featured');
		$this->getCacheService()->addItem('special_collection_featured', $cache);
		
		$latestVideos = $this->getVideoTable()->fetchLatest(8);
		$cache = array();
		foreach ($latestVideos as $row)
		{
			$cache[] = $row;
		}
		
		$this->getCacheService()->removeItem('special_collection_latest');
		$this->getCacheService()->addItem('special_collection_latest', $cache);
		
		$mostPopular = $this->getVideoTable()->fetchMostPopular(8);
		$cache = array();
		foreach ($mostPopular as $row)
		{
			$cache[] = $row;
		}
		$this->getCacheService()->removeItem('special_collection_popular');
		$this->getCacheService()->addItem('special_collection_popular', $cache);
	}

	public function managevideoAction()
	{
		$this->layout()->setVariable('area', 'videos');

		$videoid = $this->params()->fromRoute('id', false);   // From POST
		$form = new VideoForm( $this->getCollectionTable()->fetchAllWithoutDepth() , $this->getCollectionTable()->fetchSpecial() );

		$request = $this->getRequest();
		if ($request->isPost()) {
			$data = $request->getPost();
			$video = ($data['videoid']) ? $this->getVideoTable()->getVideo($data['videoid']) : new Video();
			$form->setInputFilter($form->getInputFilter());
			$form->setData($data);
			if ($form->isValid()) {
				$video->mergeArray($form->getData());
				$this->getVideoTable()->saveVideo($video);
				$this->updateVideoCollectionCache();

				// Redirect to list of albums
				return $this->redirect()->toRoute('admin', array('action' => 'managevideos'), array('collectionid' => $data['collectionid']));
			}
		} else {
			if ($videoid) $form->bind($this->getVideoTable()->getVideo($videoid));
		}
		return array('form' => $form);
	}

	public function massupdateAction()
	{
		$collectionid = $this->params()->fromPost('collectionid');
		$action = $this->params()->fromPost('action');
		$order = $this->params()->fromPost('order');
		$selected = $this->params()->fromPost('selected');
		if ($action == 'delete')
		{
			foreach ($selected as $videoid)
			{
				$this->getVideoTable()->deleteVideo($videoid);
			}
			$this->getCollectionTable()->updateVideoCount($collectionid, $this->getVideoTable()->getVideoCountForCollection($collection));
		} else if ($action == 'order') {
			$collection = $this->getCollectionTable()->getCollection($collectionid);
			foreach ($order as $videoid => $value)
			{
				$video = $this->getVideoTable()->getVideo($videoid);
				if (!$collection->special)
				{
					$video->order = (empty($value)) ? new \Zend\Db\Sql\Expression("NULL") : $value;
				} else {
					$video->specialorder = (empty($value)) ? new \Zend\Db\Sql\Expression("NULL") : $value;
				}

				$this->getVideoTable()->saveVideo($video);
			}
		} else {
			foreach ($selected as $videoid)
			{
				$video = $this->getVideoTable()->getVideo($videoid);
				$video->collectionid = $action;
				$this->getVideoTable()->saveVideo($video);
			}
		}
		$this->updateVideoCollectionCache();

		return $this->redirect()->toRoute('admin', array('action' => 'managevideos', 'id' => $collectionid));
	}
	
	public function deletevideoAction()
	{
		$id = $this->params()->fromRoute('id', false);   // From POST
		$this->getVideoTable()->deleteVideo($id);

		return $this->redirect()->toRoute('admin', array('action' => 'managevideos'));
	}
	

	public function managevideosAction()
	{
		$this->layout()->setVariable('area', 'videos');

		$collectionid = $this->params()->fromQuery('id', false);
		if (!$collectionid) $collectionid = $this->params()->fromRoute('id', false);
		

		$vars = array('collectionid' => $collectionid, 'collection' => false, 'videos' => false);
		$vars['collections'] = $this->getCollectionTable()->fetchAllWithoutDepth(false);
		if ($collectionid)
		{
			$vars['collection'] = $this->getCollectionTable()->getCollection($collectionid);
			$vars['videos'] = $this->getVideoTable()->getVideosForCollection($collectionid, (bool)$vars['collection']->special);
		}

		return $vars;
	}

	public function importvideosAction()
	{
		$this->layout()->setVariable('area', 'none');

		$imports = $this->params()->fromPost('video');   // From POST
		$collection = $this->params()->fromPost('collection');   // From POST
		$data = $this->params()->fromPost('data');   // From POST
		$date = new \DateTime();
		$timestamp = $date->getTimestamp();

		$imports = $this->getVideoTable()->removeDuplicateIDs($imports);

		foreach ($imports as $import)
		{
			$url = "http://www.theonlytutorials.com/img/logo.png";
			$name = basename($videoData['preview']);
			file_put_contents("/home/combathub/combathub.com/img/videos/$name", file_get_contents($videoData['preview']));
			
			$videoData = unserialize($data["$import"]);
			$videoObj = new Video();
			$videoObj->exchangeArray(
				array(
					'youtubeid' => $import, 
					'title' => $videoData['title'], 
					'description' => $videoData['description'], 
					'collectionid' => $collection, 
					'preview' => 'http://www.combathub.com/images/videos/' . $name,
					'dateline' => $timestamp
				)
			);
			$this->getVideoTable()->saveVideo($videoObj);
		}

		$this->getCollectionTable()->updateVideoCount($collection, $this->getVideoTable()->getVideoCountForCollection($collection));
		$this->updateVideoCollectionCache();
		return $this->redirect()->toRoute('admin', array('action' => 'managevideos', 'id' => $collection));
	}


	public function discoverAction()
	{
		$this->layout()->setVariable('area', 'videos');

		$searchq = $this->params()->fromQuery('search');  // From GET
		$perpage = 50;
		$pageNumber = $this->params()->fromQuery('page', 1)-1;  // From GET
		$vars = array('area' => 'videos', 'link' => 'findvideos', 'page' => $pageNumber+1, 'query' => $searchq, 'area' => 'videos', 'videos' => null);
		if ($searchq)
		{
			$yt = new \Zend_Gdata_YouTube();
			$query = $yt->newVideoQuery();
			$query->videoQuery = $searchq;
			$query->startIndex = ($pageNumber * $perpage)+1;
			$query->maxResults = $perpage;
			$query->orderBy = 'viewCount';
			$vars['qurl'] = $query->queryUrl;
			$unfilteredVideos = $yt->getVideoFeed($query);
			$vars['videos'] = array();
			foreach ($unfilteredVideos as $video)
			{
				if (!$vid = $this->getVideoTable()->getVideoByYouTubeId($video->getVideoId()))
				{
					$vars['videos'][] = $video;
				}
			}
			$vars['collections'] = $this->getCollectionTable()->fetchAllWithoutDepth();
		}
		return new ViewModel($vars);
	}

	public function listeventsAction()
	{
		$this->layout()->setVariable('area', 'events');
		return new ViewModel(array('events' => $this->getEventTable()->fetchAll()));
	}


	public function manageeventAction()
	{
		$this->layout()->setVariable('area', 'events');

		$eventid = $this->params()->fromRoute('id', false);   // From POST
		$form = new EventForm();

		$request = $this->getRequest();
		if ($request->isPost()) {
			$data = $request->getPost();
			$event = ($data['eventid']) ? $this->getEventTable()->getEvent($data['eventid']) : new Event();
			$form->setInputFilter($form->getInputFilter());
			$form->setData($data);
			if ($form->isValid()) {
				$event->exchangeArray($form->getData());
				$this->getEventTable()->saveEvent($event);

				$this->getCacheService()->removeItem('events');
				$this->getCacheService()->addItem('events', $this->getEventTable()->fetchCacheable());

				// Redirect to list of albums
				return $this->redirect()->toRoute('admin', array('action' => 'listevents'));
			}
		} else {
			if ($eventid) $form->bind($this->getEventTable()->getEvent($eventid));
		}
		return array('form' => $form, 'new' => !(bool)$eventid);
	}

	public function deleteeventAction()
	{
		$eventid = $this->params()->fromRoute('id', false);   // From POST
		$this->getEventTable()->deleteEvent($eventid);

		$this->getCacheService()->removeItem('events');
		$this->getCacheService()->addItem('events', $this->getEventTable()->fetchCacheable());

		return $this->redirect()->toRoute('admin', array('action' => 'listevents'));
	}

	public function listusersAction()
	{
		$this->layout()->setVariable('area', 'users');
		return new ViewModel(array('users' => $this->getUserTable()->fetchAll()));
	}


	public function manageuserAction()
	{
		$this->layout()->setVariable('area', 'users');

		$userid = $this->params()->fromRoute('id', false);   // From POST
		$form = new UserForm();

		$request = $this->getRequest();
		if ($request->isPost()) {
			$data = $request->getPost();
			$user = ($data['userid']) ? $this->getUserTable()->getUser($data['userid']) : new User();
			$form->setInputFilter($form->getInputFilter());
			$form->setData($data);
			if ($form->isValid()) {
				$user->exchangeArray($form->getData());
				$this->getUserTable()->saveUser($user);

				return $this->redirect()->toRoute('admin', array('action' => 'listusers'));
			}
		} else {
			if ($userid) $form->bind($this->getUserTable()->getUser($userid));
		}
		return array('form' => $form, 'new' => !(bool)$userid);
	}

	public function deleteuserAction()
	{
		$userid = $this->params()->fromRoute('id', false);   // From POST
		$this->getUserTable()->deleteUser($userid);

		return $this->redirect()->toRoute('admin', array('action' => 'listusers'));
	}


	public function listpollsAction()
	{
		$this->layout()->setVariable('area', 'polls');

		return new ViewModel(array('polls' => $this->getPollTable()->fetchAll()));
	}

	private function refreshpollcache($poll)
	{
		if (!$poll->active) return;
		$this->getCacheService()->removeItem('poll');
		$this->getCacheService()->addItem('poll', $this->getPollOptionTable()->createCache($poll));
	}

	public function managepollAction()
	{
		$this->layout()->setVariable('area', 'polls');

		$pollid = $this->params()->fromRoute('id', false);   // From POST
		$form = new PollForm();

		$request = $this->getRequest();
		if ($request->isPost()) {
			$data = $request->getPost();
			$poll = ($data['pollid']) ? $this->getPollTable()->getPoll($data['pollid']) : new Poll();
			$form->setInputFilter($form->getInputFilter());
			$form->setData($data);
			if ($form->isValid()) {
				if ($data['active']) $this->getPollTable()->resetActive();
				$poll->exchangeArray($form->getData());
				$this->getPollTable()->savePoll($poll);

				$this->refreshpollcache($poll);
				// Redirect to list of albums
				return $this->redirect()->toRoute('admin', array('action' => 'listpolls'));
			}
		} else {
			if ($pollid) $form->bind($this->getPollTable()->getPoll($pollid));
		}
		return array('form' => $form, 'new' => !(bool)$pollid);
	}

	public function deletepollAction()
	{
		$id = $this->params()->fromRoute('id', false);   // From POST
		$this->getPollTable()->deletePoll($id);

		return $this->redirect()->toRoute('admin', array('action' => 'listpolls'));
	}

	public function deletepolloptionAction()
	{
		$id = $this->params()->fromRoute('id', false);   // From POST
		$opt = $this->getPollOptionTable()->getOption($id);
		$this->getPollOptionTable()->deleteOption($id);

		return $this->redirect()->toRoute('admin', array('action' => 'listpolloptions', 'id' => $opt->pollid));
	}

	public function listpolloptionsAction()
	{
		$pollid = $this->params()->fromRoute('id', false);   // From POST
		$form = new PollOptionForm();
		$form->setData(array('pollid' => $pollid));
		return new ViewModel(
			array('options' => $this->getPollOptionTable()->fetchAll($pollid), 'form' => $form, 'poll' => $this->getPollTable()->getPoll($pollid))
		);
	}

	public function managepolloptionAction()
	{
		$this->layout()->setVariable('area', 'polls');

		$pollid = $this->params()->fromRoute('id', false);   // From POST
		$form = new PollOptionForm();

		$request = $this->getRequest();
		if ($request->isPost()) {
			$data = $request->getPost();
			$poll = ($data['answerid']) ? $this->getPollOptionTable()->getOption($data['answerid']) : new PollOption();
			$form->setInputFilter($form->getInputFilter());
			$form->setData($data);
			if ($form->isValid()) {
				$poll->exchangeArray($form->getData());
				$optionid = $this->getPollOptionTable()->saveOption($poll);

				$this->refreshpollcache($this->getPollTable()->getPoll($poll->pollid));

				// Redirect to list of albums
				return $this->redirect()->toRoute('admin', array('action' => 'listpolloptions', 'id' => $data['pollid']));
			}
		} else {
			if ($pollid) $form->bind($this->getPollOptionTable()->getOption($pollid));
		}
		return array('form' => $form);
	}


}
