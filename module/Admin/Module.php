<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Admin;

use Zend\Mvc\ModuleRouteListener;
use Zend\ModuleManager\ModuleManager;
use Zend\Mvc\MvcEvent;

use Zend\Authentication\AuthenticationService;
use Zend\Db\Adapter\Adapter as DbAdapter;
use Zend\Authentication\Adapter\DbTable as AuthAdapter;

use Application\Video\Model\Video;
use Application\Video\Model\VideoTable;
use Application\Collection\Model\Collection;
use Application\Collection\Model\CollectionTable;
use Application\Event\Model\Event;
use Application\Event\Model\EventTable;
use Application\Poll\Model\Poll;
use Application\Poll\Model\PollTable;
use Application\Poll\Model\PollOption;
use Application\Poll\Model\PollOptionTable;
use Application\User\Model\User;
use Application\User\Model\UserTable;


use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class Module
{
	public function init(ModuleManager $moduleManager)
	{
        	$sharedEvents = $moduleManager->getEventManager()->getSharedManager();
        	$sharedEvents->attach(__NAMESPACE__, 'dispatch', function($e) {
	            // This event will only be fired when an ActionController under the MyModule namespace is dispatched.
	            $controller = $e->getTarget();
	            $controller->layout('layout/admin-layout.phtml');
	        }, 100);
	}

    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/admin.module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'VideoTable' =>  function($sm) {
                    $tableGateway = $sm->get('VideoTableGateway');
                    $table = new \Application\Video\Model\VideoTable($tableGateway);
                    return $table;
                },
                'VideoTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new \Application\Video\Model\Video());
                    return new TableGateway('video', $dbAdapter, null, $resultSetPrototype);
                },
                'CollectionTable' =>  function($sm) {
                    $tableGateway = $sm->get('CollectionTableGateway');
                    $table = new \Application\Collection\Model\CollectionTable($tableGateway);
                    return $table;
                },
                'CollectionTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new \Application\Collection\Model\Collection());
                    return new TableGateway('collection', $dbAdapter, null, $resultSetPrototype);
                },
                'EventTable' =>  function($sm) {
                    $tableGateway = $sm->get('EventTableGateway');
                    $table = new \Application\Event\Model\EventTable($tableGateway);
                    return $table;
                },
                'EventTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new \Application\Event\Model\Event());
                    return new TableGateway('event', $dbAdapter, null, $resultSetPrototype);
                },
                'PollTable' =>  function($sm) {
                    $tableGateway = $sm->get('PollTableGateway');
                    $table = new \Application\Poll\Model\PollTable($tableGateway);
                    return $table;
                },
                'PollTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new \Application\Poll\Model\Poll());
                    return new TableGateway('poll', $dbAdapter, null, $resultSetPrototype);
                },
                'PollOptionTable' =>  function($sm) {
                    $tableGateway = $sm->get('PollOptionTableGateway');
                    $table = new \Application\Poll\Model\PollOptionTable($tableGateway);
                    return $table;
                },
                'PollOptionTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new \Application\Poll\Model\PollOption());
                    return new TableGateway('poll_answers', $dbAdapter, null, $resultSetPrototype);
                },
                'UserTable' =>  function($sm) {
                    $tableGateway = $sm->get('UserTableGateway');
                    $table = new \Application\User\Model\UserTable($tableGateway);
                    return $table;
                },
                'UserTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new \Application\User\Model\User());
                    return new TableGateway('users', $dbAdapter, null, $resultSetPrototype);
                },
		'AuthService' =>  function($sm) {
			$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
			$auth = new AuthenticationService();
			$authAdapter = new AuthAdapter($dbAdapter,
                               'users',
                               'username',
                               'password',
				'MD5(?)'
                               );
			$auth->setAdapter($authAdapter);
                    return $auth;
		},
            ),
        );
    }

}
