<?php
namespace Application\Event\Model;

class Event
{
	public $eventid;
	public $title;
	public $description;
	public $dateline;
	public $realDateline;
	public $url;

	public function exchangeArray($data)
	{
		$this->realDateline  = (isset($data['dateline'])) ? $data['dateline'] : null;
		if (is_numeric($data['dateline']))
		{
			$data['dateline'] = date("m/d/y", $data['dateline']);
		}

		$this->eventid  = (isset($data['eventid'])) ? $data['eventid'] : null;
		$this->title  = (isset($data['title'])) ? $data['title'] : null;
		$this->description  = (isset($data['description'])) ? $data['description'] : null;
		$this->dateline  = (isset($data['dateline'])) ? $data['dateline'] : null;
		$this->url  = (isset($data['url'])) ? $data['url'] : null;
	}

	public function getArrayCopy()
	{
		$vars = get_object_vars($this);
		unset($vars['realDateline']);
		return $vars;
	}
}