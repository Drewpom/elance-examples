<?php
namespace Application\Event\Model;

use Zend\Db\TableGateway\TableGateway;

class EventTable
{
	protected $tableGateway;

	public function __construct(TableGateway $tableGateway)
	{
		$this->tableGateway = $tableGateway;
	}

	public function fetchAll()
	{
		$select = $this->tableGateway->getSql()->select();
		$select->order('dateline ASC');
		$resultSet = $this->tableGateway->selectWith($select);
		return $resultSet;
	}

	public function fetchCacheable()
	{
		$data = $this->fetchAll();
		$realData = array();
		foreach ($data as $row)
		{
			$day = date('j', $row->realDateline);
			$month = date('n', $row->realDateline);
			$year = date('Y', $row->realDateline);
			$key = intval($day . $month . $year);
			$realData["$key"] = array('eventid' => $row->eventid, 'url' => $row->url, 'title' => $row->title);
		}
		
		return $realData;
	}


	public function getEvent($id)
	{
		$id  = (int) $id;
		$rowset = $this->tableGateway->select(array('eventid' => $id));
		$row = $rowset->current();
		if (!$row) {
			throw new \Exception("Could not find row $id");
		}
		return $row;
	}

	private function createUrl($string, $force_lowercase = true, $anal = false) {
		$strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]",
			"}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
			"â€”", "â€“", ",", "<", ".", ">", "/", "?");
		$clean = trim(str_replace($strip, "", strip_tags($string)));
		$clean = preg_replace('/\s+/', "-", $clean);
		$clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean ;
		return ($force_lowercase) ?
			(function_exists('mb_strtolower')) ?
			mb_strtolower($clean, 'UTF-8') :
			strtolower($clean) :
			$clean;
	}


	public function saveEvent(Event $event)
	{
		$data = $event->getArrayCopy();
		if (!is_int($data['dateline']))
		{
			$data['dateline'] = strtotime($data['dateline']);
		}

		$data['url'] = $this->createUrl($data['title']);

		$id = (int)$event->eventid;
		if ($id == 0) {
			$this->tableGateway->insert($data);
		} else {
			if ($this->getEvent($id)) {
				$this->tableGateway->update($data, array('eventid' => $id));
			} else {
				throw new \Exception('Video id does not exist');
			}
		}
	}

	public function deleteEvent($id)
	{
		$this->tableGateway->delete(array('eventid' => $id));
	}
}
