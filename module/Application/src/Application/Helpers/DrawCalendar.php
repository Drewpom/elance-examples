<?php
namespace Application\Helpers;

use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorInterface as ServiceLocator;

class DrawCalendar extends AbstractHelper
{
	protected $serviceLocator;
	protected $eventData;

	public function getData()
	{
		if (!$this->eventData)
		{
			$cache = $this->serviceLocator->get('cache');
			$this->eventData = $cache->getItem('events');
		}
		
	}

	public function __invoke($offsetMonth = 0)
	{
		$this->getData();
		$month = date('m')+$offsetMonth; 
		$year = date('Y');
		if ($month > 12)
		{
			$month = $month-12;
			$year++;
		}

		$first_day = mktime(0,0,0,$month, 1, $year) ; 
		$title = date('F', $first_day) ; 

		$day_of_week = date('D', $first_day) ; 

		switch($day_of_week){ 
			case "Sun": $blank = 0; break; 
			case "Mon": $blank = 1; break; 
			case "Tue": $blank = 2; break; 
			case "Wed": $blank = 3; break; 
			case "Thu": $blank = 4; break; 
			case "Fri": $blank = 5; break; 
			case "Sat": $blank = 6; break; 
		}

		$days_in_month = cal_days_in_month(0, $month, $year) ; 
		$html = '<table cellspacing="0" cellpadding="0" align="center" border="1" bordercolor="#646464" width="200" class="calendar">';
		$html .= "<tr><th colspan=7> $title $year </th></tr>";
		$html .= "<tr><td>S</td><td>M</td><td>T</td><td>W</td><td>T</td><td>F</td><td>S</td></tr>";
		$day_count = 1;
		$html .= "<tr>";
		while ( $blank > 0 ) 
		{ 
			$html .= "<td></td>"; 
			$blank = $blank-1; 
			$day_count++;
		} 
		$day_num = 1;
		$dayOfMonth = 1;
		while ( $day_num <= $days_in_month ) 
		{
			$key = intval($dayOfMonth . $month.$year);
			if (array_key_exists($key, $this->eventData))
			{
				$url = $this->serviceLocator->get('router')->assemble(array('id' => $this->eventData["$key"]['eventid'], 'url' => $this->eventData["$key"]['url']), array('name' => 'event'));
				$html .= '<td> <a href="'.$url.'">'.$day_num.'</a> </td>'; 
			} else {
				$html .= "<td> $day_num </td>"; 
			}
			$day_num++; 
			$day_count++;
			$dayOfMonth++;
			if (($day_count % 8) == 0)
			{
				$html .= "</tr><tr>";
				$day_count = 1;
			}
		} 

		while ( $day_count >1 && $day_count <=7 ) 
		{ 
			$html .= "<td> </td>"; 
			$day_count++; 
		}

 		$html .= "</tr></table>"; 
		return $html;
	}


	public function __construct(ServiceLocator $serviceLocator)
	{
		$this->serviceLocator = $serviceLocator;
	}
}