<?php
namespace Application\Helpers;

use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorInterface as ServiceLocator;

class GetCollections extends AbstractHelper
{
	protected $serviceLocator;
	protected $collectionData;

	public function getData()
	{
		if (!$this->collectionData)
		{
			$cache = $this->serviceLocator->get('cache');
			$this->collectionData = $cache->getItem('collections');
		}
		
	}

	public function __invoke()
	{
		$this->getData();
		return $this->getListHTML($this->collectionData);
	}

	public function getListHTML($data)
	{
		$HTML = '<ul>';
		foreach ($data as $row)
		{
			$url = $this->serviceLocator->get('router')->assemble(array('id' => $row['collectionid'], 'url' => $row['url']), array('name' => 'collection'));
			$HTML .= '<li><a href="'. $url .'">' . $row['title'] . '</a>';
			if (array_key_exists('children', $row) AND is_array($row['children'])) $HTML .= $this->getListHTML($row['children']);
			$HTML .= '</li>';
		}
		$HTML .= '</ul>';
		return $HTML;
	}

	public function __construct(ServiceLocator $serviceLocator)
	{
		$this->serviceLocator = $serviceLocator;
	}
}