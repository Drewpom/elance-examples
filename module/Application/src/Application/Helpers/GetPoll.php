<?php
namespace Application\Helpers;

use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorInterface as ServiceLocator;
use Zend\Session\Container;

class GetPoll extends AbstractHelper
{
	protected $serviceLocator;
	protected $pollData;

	public function getData()
	{
		if (!$this->pollData)
		{
			$cache = $this->serviceLocator->get('cache');
			$this->pollData = $cache->getItem('poll');
		}
		
	}

	public function __invoke()
	{
		$container = new Container('myapp');
			$this->getData();

		$html = '<h4>'.$this->pollData['title'].'</h4>';
		if (!$container->hasVoted)
		{
			$options = '';
			
			foreach ($this->pollData['options'] as $id => $value)
			{
				$options .= '<div>
					<input type="radio" name="poll" value="'.$id.'" id="opt'.$id.'" /><label for="opt'.$id.'">'.$value.'</label>
				</div>';
			}
	
			$url = $this->serviceLocator->get('router')->assemble(array('controller' => 'index', 'action' => 'submitpoll'), array('name' => 'application/default'));
			$html .= '
				<form action="'.$url.'" method="post">
					' . $options . '
					<div id="poll-submit-row">
						<input type="submit" name="submit" value="Vote" class="poll-submit" />
					</div>
				</form>
			';
			return $html;
		} else {
			$cache = $this->serviceLocator->get('cache');
			$data = $cache->getItem('polldata');
			$html .= "<div id='mychart'></div>
    <script type=\"text/javascript\" src=\"https://www.google.com/jsapi\"></script>
    <script type=\"text/javascript\">
      google.load(\"visualization\", \"1\", {packages:[\"corechart\"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          " . $data['data'] . "
        ]);

        var options = {
        	backgroundColor: 'transparent', legend: {textStyle: {color: '#fff'}}
        };

        var chart = new google.visualization.PieChart(document.getElementById('mychart'));
        chart.draw(data, options);
      }
</script>";
			return $html;
		}
	}

	public function __construct(ServiceLocator $serviceLocator)
	{
		$this->serviceLocator = $serviceLocator;
	}
}