<?php
namespace Application\Helpers;

use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorInterface as ServiceLocator;

class GetEvents extends AbstractHelper
{
	protected $serviceLocator;
	protected $eventData;

	public function getData()
	{
		if (!$this->eventData)
		{
			$cache = $this->serviceLocator->get('cache');
			$this->eventData = $cache->getItem('events');
		}
		
	}

	public function __invoke($length)
	{
		$this->getData();
		return array_slice($this->eventData, 0, $length);
	}

	public function __construct(ServiceLocator $serviceLocator)
	{
		$this->serviceLocator = $serviceLocator;
	}
}