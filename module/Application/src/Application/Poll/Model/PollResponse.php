<?php
namespace Application\Poll\Model;

class PollResponse
{
	public $responseid;
	public $pollid;
	public $answerid;

	public function exchangeArray($data)
	{
		$this->responseid     = (isset($data['responseid'])) ? $data['responseid'] : null;
		$this->pollid     = (isset($data['pollid'])) ? $data['pollid'] : null;
		$this->answerid     = (isset($data['answerid'])) ? $data['answerid'] : null;
	}

	public function getArrayCopy()
	{
		return get_object_vars($this);
	}
}