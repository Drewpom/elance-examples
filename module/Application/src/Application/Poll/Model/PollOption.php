<?php
namespace Application\Poll\Model;

class PollOption
{
	public $answerid;
	public $pollid;
	public $title;
	public $votes;

	public function exchangeArray($data)
	{
		$this->answerid     = (isset($data['answerid'])) ? $data['answerid'] : null;
		$this->pollid     = (isset($data['pollid'])) ? $data['pollid'] : null;
		$this->title     = (isset($data['title'])) ? $data['title'] : null;
		$this->votes     = (isset($data['votes'])) ? $data['votes'] : 0;
	}

	public function getArrayCopy()
	{
		return get_object_vars($this);
	}
}