<?php
namespace Application\Poll\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Where;
use Application\Poll\Model\Poll;


class PollResponseTable
{
	protected $tableGateway;

	public function __construct(TableGateway $tableGateway)
	{
		$this->tableGateway = $tableGateway;
	}

	public function fetchAll($pollid)
	{
		$select = $this->tableGateway->getSql()->select();
		$select->where(array('pollid' => $pollid));
		$resultSet = $this->tableGateway->selectWith($select);
		return $resultSet;
	}

	public function getResponse($id)
	{
		$id  = (int) $id;
		$rowset = $this->tableGateway->select(array('responseid' => $id));
		$row = $rowset->current();
		if (!$row) {
			throw new \Exception("Could not find row $id");
		}
		return $row;
	}

	public function saveResponse(PollResponse $response)
	{
		$data = $response->getArrayCopy();

		$id = (int)$response->responseid;
		if ($id == 0) {
			$this->tableGateway->insert($data);
			return $this->tableGateway->lastInsertValue;
		} else {
			if ($this->getResponse($id)) {
				$this->tableGateway->update($data, array('responseid' => $id));
				return $id;
			} else {
				throw new \Exception('Poll Option id does not exist');
			}
		}
	}

	public function deleteResponse($id)
	{
		$this->tableGateway->delete(array('responseid' => $id));
	}
}
