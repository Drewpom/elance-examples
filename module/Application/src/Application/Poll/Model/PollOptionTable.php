<?php
namespace Application\Poll\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Where;
use Application\Poll\Model\Poll;


class PollOptionTable
{
	protected $tableGateway;

	public function __construct(TableGateway $tableGateway)
	{
		$this->tableGateway = $tableGateway;
	}

	public function fetchAll($pollid)
	{
		$select = $this->tableGateway->getSql()->select();
		$select->where(array('pollid' => $pollid));
		$resultSet = $this->tableGateway->selectWith($select);
		return $resultSet;
	}

	public function createDataCache(Poll $poll)
	{
		$options = $this->fetchAll($poll->pollid);
		$optionData = array("['Answer', 'Votes']");
		foreach ($options as $row)
		{
			$optionData[] = '["'.$row->title.'", '. $row->votes . ']';
		}
		$optionData = implode(',', $optionData);

		return array(
			'pollid' => $poll->pollid,
			'title' => $poll->question,
			'data' => $optionData
		);
	}

	public function createCache(Poll $poll)
	{
		$options = $this->fetchAll($poll->pollid);
		$opts = array();
		foreach ($options as $row)
		{
			$id = $row->answerid;
			$opts[$id] = $row->title;
		}

		return array(
			'pollid' => $poll->pollid,
			'title' => $poll->question,
			'options' => $opts
		);
	}

	public function getOption($id)
	{
		$id  = (int) $id;
		$rowset = $this->tableGateway->select(array('answerid' => $id));
		$row = $rowset->current();
		if (!$row) {
			throw new \Exception("Could not find row $id");
		}
		return $row;
	}

	public function saveOption(PollOption $opt)
	{
		$data = $opt->getArrayCopy();

		$id = (int)$opt->answerid;
		if ($id == 0) {
			$this->tableGateway->insert($data);
			return $this->tableGateway->lastInsertValue;
		} else {
			if ($this->getOption($id)) {
				$this->tableGateway->update($data, array('answerid' => $id));
				return $id;
			} else {
				throw new \Exception('Poll Option id does not exist');
			}
		}
	}

	public function deleteOption($id)
	{
		$this->tableGateway->delete(array('answerid' => $id));
	}
}
