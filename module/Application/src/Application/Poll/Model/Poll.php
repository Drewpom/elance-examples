<?php
namespace Application\Poll\Model;

class Poll
{
	public $pollid;
	public $active;
	public $question;

	public function exchangeArray($data)
	{
		$this->pollid     = (isset($data['pollid'])) ? $data['pollid'] : null;
		$this->active     = (isset($data['active'])) ? $data['active'] : null;
		$this->question     = (isset($data['question'])) ? $data['question'] : null;
	}

	public function getArrayCopy()
	{
		return get_object_vars($this);
	}
}