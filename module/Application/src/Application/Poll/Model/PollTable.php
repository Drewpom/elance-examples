<?php
namespace Application\Poll\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Where;

class PollTable
{
	protected $tableGateway;

	public function __construct(TableGateway $tableGateway)
	{
		$this->tableGateway = $tableGateway;
	}

	public function fetchAll()
	{
		$resultSet = $this->tableGateway->select();
		return $resultSet;
	}

	public function getPoll($id)
	{
		$id  = (int) $id;
		$rowset = $this->tableGateway->select(array('pollid' => $id));
		$row = $rowset->current();
		if (!$row) {
			throw new \Exception("Could not find row $id");
		}
		return $row;
	}

	public function resetActive()
	{
		$this->tableGateway->update(array('active' => 0));
	}

	public function savePoll(Poll $poll)
	{
		$data = $poll->getArrayCopy();

		$id = (int)$poll->pollid;
		if ($id == 0) {
			$this->tableGateway->insert($data);
		} else {
			if ($this->getPoll($id)) {
				$this->tableGateway->update($data, array('pollid' => $id));
			} else {
				throw new \Exception('Video id does not exist');
			}
		}
	}

	public function deletePoll($id)
	{
		$this->tableGateway->delete(array('pollid' => $id));
	}
}
