<?php

namespace Application\Poll\Form;

use Zend\Form\Form;
use Zend\InputFilter\Factory as InputFactory;     // <-- Add this import
use Zend\InputFilter\InputFilter;                 // <-- Add this import
use Zend\InputFilter\InputFilterAwareInterface;   // <-- Add this import
use Zend\InputFilter\InputFilterInterface;        // <-- Add this import

class PollForm extends Form
{
	protected $inputFilter;                       // <-- Add this variable

	public function __construct()
	{
		// we want to ignore the name passed
		parent::__construct('poll');

		$this->setAttribute('method', 'post');
		$this->add(array(
			'name' => 'pollid',
			'attributes' => array(
				'type'  => 'hidden',
			),
		));

		$this->add(array(
			'name' => 'question',
			'attributes' => array(
				'type'  => 'text',
			),
			'options' => array(
				'label' => 'Question',
			),
		));

		$this->add(array(
			'type' => 'Zend\Form\Element\Radio', 
			'name' => 'active',
			'options' => array(
				'label' => 'Current Poll?',
				'value_options' => array(0 => 'No', 1 => 'Yes')
			),
		));


		$this->add(array(
			'name' => 'submit',
			'attributes' => array(
				'type'  => 'submit',
				'value' => 'Go',
				'id' => 'submitbutton',
			),
		));
	}

	public function getInputFilter()
	{
		if (!$this->inputFilter) {
			$inputFilter = new InputFilter();

			$inputFilter->add(array(
				'name'     => 'pollid',
				'required' => true,
				'filters'  => array(
					array('name' => 'Int'),
				),
			));

			$inputFilter->add(array(
				'name'     => 'question',
				'required' => true,
				'filters'  => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
					array(
						'name'    => 'StringLength',
						'options' => array(
							'encoding' => 'UTF-8',
							'min'      => 1,
							'max'      => 100,
						),
					),
				),
			));

			$inputFilter->add(array(
				'name' => 'active',
				'required' => true,
				'filters'  => array(
					array('name' => 'Int'),
				),
			));

			$this->inputFilter = $inputFilter;
		}

		return $this->inputFilter;
	}

}
