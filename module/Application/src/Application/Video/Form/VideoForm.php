<?php

namespace Application\Video\Form;

use Zend\Form\Form;
use Zend\InputFilter\Factory as InputFactory;     // <-- Add this import
use Zend\InputFilter\InputFilter;                 // <-- Add this import
use Zend\InputFilter\InputFilterAwareInterface;   // <-- Add this import
use Zend\InputFilter\InputFilterInterface;        // <-- Add this import

class VideoForm extends Form
{
	private $collectionData;
	private $specialData;
	protected $inputFilter;                       // <-- Add this variable

	public function __construct($collectionData, $specialData)
	{
		// we want to ignore the name passed
		parent::__construct('video');
		$this->collectionData = $collectionData;
		$this->specialData = $specialData;

		$this->setAttribute('method', 'post');
		$this->add(array(
			'name' => 'videoid',
			'attributes' => array(
				'type'  => 'hidden',
			),
		));

		$this->add(array(
			'name' => 'title',
			'attributes' => array(
				'type'  => 'text',
			),
			'options' => array(
				'label' => 'Title',
			),
		));

		$options = array();

		foreach ($this->collectionData as $row)
		{
			$options["{$row->collectionid}"] = str_repeat(' -- ', $row->depth) . $row->title;
		}


		$this->add(array(
			'type' => 'Zend\Form\Element\Select', 
			'name' => 'collectionid',
			'options' => array(
				'label' => 'Collection',
				'value_options' => $options
			),
		));

		$options = array(0 => 'None');

		foreach ($this->specialData as $row)
		{
			$options["{$row->collectionid}"] = str_repeat(' -- ', $row->depth) . $row->title;
		}


		$this->add(array(
			'type' => 'Zend\Form\Element\Select', 
			'name' => 'specialcollectionid',
			'options' => array(
				'label' => 'Home Page Collection',
				'value_options' => $options
			),
		));

		$this->add(array(
			'type' => 'Zend\Form\Element\Textarea', 
			'name' => 'description',
			'options' => array(
				'label' => 'Description'
			),
		));



		$this->add(array(
			'name' => 'submit',
			'attributes' => array(
				'type'  => 'submit',
				'value' => 'Go',
				'id' => 'submitbutton',
			),
		));
	}

	public function getInputFilter()
	{
		if (!$this->inputFilter) {
			$inputFilter = new InputFilter();

			$inputFilter->add(array(
				'name'     => 'videoid',
				'required' => true,
				'filters'  => array(
					array('name' => 'Int'),
				),
			));

			$inputFilter->add(array(
				'name'     => 'title',
				'required' => true,
				'filters'  => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
					array(
						'name'    => 'StringLength',
						'options' => array(
							'encoding' => 'UTF-8',
							'min'      => 1,
							'max'      => 100,
						),
					),
				),
			));

			$inputFilter->add(array(
				'name'     => 'description',
				'required' => true,
				'filters'  => array(
					array('name' => 'StringTrim'),
				),
			));

			$inputFilter->add(array(
				'name' => 'collectionid',
				'required' => true,
				'filters'  => array(
					array('name' => 'Int'),
				),
			));

			$inputFilter->add(array(
				'name' => 'specialcollectionid',
				'required' => false,
				'filters'  => array(
					array('name' => 'Int'),
				),
			));


			$this->inputFilter = $inputFilter;
		}

		return $this->inputFilter;
	}

}
