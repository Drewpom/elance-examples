<?php
namespace Application\Video\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Where;

class VideoTable
{
	protected $tableGateway;

	public function __construct(TableGateway $tableGateway)
	{
		$this->tableGateway = $tableGateway;
	}

	public function fetchAll()
	{
		$resultSet = $this->tableGateway->select();
		return $resultSet;
	}

	public function fetchLatest($limit)
	{
		$select = $this->tableGateway->getSql()->select();
		$select->order('dateline DESC')->limit($limit);
		$resultSet = $this->tableGateway->selectWith($select);
		return $resultSet;
	}

	public function fetchMostPopular($limit)
	{
		$select = $this->tableGateway->getSql()->select();
		$select->order('views DESC')->limit($limit);
		$resultSet = $this->tableGateway->selectWith($select);
		return $resultSet;
	}

	public function search($q)
	{
		$select = $this->tableGateway->getSql()->select();
		$select->where("title LIKE '%" . $q . "%' OR description LIKE '%" . $q . "%'");
		$resultSet = $this->tableGateway->selectWith($select);
		return $resultSet;
	}

	public function increaseViewCount($videoid)
	{
		$video = $this->getVideo($videoid);
		$video->views++;
		$this->saveVideo($video);
	}

	public function getVideosForCollection($id, $isSpecial = false)
	{
		if (is_array($id))
		{
			$id = array_map('intval',$id);	
		}
		if (!$isSpecial)
		{
			$select = $this->tableGateway->getSql()->select();
			$select->where(array('collectionid' => $id))->order(
				array(
					new \Zend\Db\Sql\Expression("ISNULL(`order`)"),'order ASC'));
		} else {
			$select = $this->tableGateway->getSql()->select();
			$select->where(array('specialcollectionid' => $id))->order(
				array(
					new \Zend\Db\Sql\Expression("ISNULL(`specialorder`)"),'specialorder ASC'));
		}
		$rowset = $this->tableGateway->selectWith($select);
		return $rowset;
	}

	public function getVideo($id)
	{
		$id  = (int) $id;
		$rowset = $this->tableGateway->select(array('videoid' => $id));
		$row = $rowset->current();
		if (!$row) {
			throw new \Exception("Could not find row $id");
		}
		return $row;
	}

	public function getVideoByYouTubeId($id)
	{
		$rowset = $this->tableGateway->select(array('youtubeid' => $id));
		return $rowset->current();
	}

	public function removeDuplicateIDs($youtubeIDs)
	{
		$rowset = $this->tableGateway->select(array('youtubeid' => $youtubeIDs));
		foreach ($rowset as $row)
		{
			$key = array_search($row->youtubeid, $youtubeIDs);
			unset($youtubeIds[$key]);
		}
		return $youtubeIDs;
	}

	public function getVideoCountForCollection($collectionid)
	{
		$select = $this->tableGateway->getSql()->select();
		$select->columns(array(
			'videoid' => new\Zend\Db\Sql\Expression('COUNT(*)')
		));
		$select->where(array('collectionid' => $collectionid));
		$result = $this->tableGateway->selectWith($select)->current()->videoid;
		return $result;
	}

	private function createUrl($string, $force_lowercase = true, $anal = false) {
		$strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]",
			"}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
			"â€”", "â€“", ",", "<", ".", ">", "/", "?");
		$clean = trim(str_replace($strip, "", strip_tags($string)));
		$clean = preg_replace('/\s+/', "-", $clean);
		$clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean ;
		return ($force_lowercase) ?
			(function_exists('mb_strtolower')) ?
			mb_strtolower($clean, 'UTF-8') :
			strtolower($clean) :
			$clean;
	}


	public function saveVideo(Video $video)
	{
		$data = $video->getArrayCopy();

		$data['url'] = $this->createUrl($data['title']);

		$id = (int)$video->videoid;
		if ($id == 0) {
			$this->tableGateway->insert($data);
		} else {
			if ($this->getVideo($id)) {
				$this->tableGateway->update($data, array('videoid' => $id));
			} else {
				throw new \Exception('Video id does not exist');
			}
		}
	}

	public function deleteVideo($id)
	{
		$this->tableGateway->delete(array('videoid' => $id));
	}
}
