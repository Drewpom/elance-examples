<?php
namespace Application\Video\Model;

class Video
{
	public $videoid;
	public $title;
	public $description;
	public $youtubeid;
	public $collectionid;
	public $specialcollectionid;
	public $views;
	public $url;
	public $preview;
	public $order;
	public $specialorder;

	public $dateline;

	public function exchangeArray($data)
	{
		$this->videoid     = (isset($data['videoid'])) ? $data['videoid'] : null;
		$this->title = (isset($data['title'])) ? $data['title'] : null;
		$this->description  = (isset($data['description'])) ? $data['description'] : null;
		$this->youtubeid  = (isset($data['youtubeid'])) ? $data['youtubeid'] : null;
		$this->collectionid  = (isset($data['collectionid'])) ? $data['collectionid'] : null;
		$this->specialcollectionid  = (isset($data['specialcollectionid'])) ? $data['specialcollectionid'] : null;
		$this->views  = (isset($data['views'])) ? $data['views'] : 0;
		$this->preview  = (isset($data['preview'])) ? $data['preview'] : null;
		$this->url  = (isset($data['url'])) ? $data['url'] : null;
		$this->dateline  = (isset($data['dateline'])) ? $data['dateline'] : null;
		$this->order  = (isset($data['order'])) ? $data['order'] : null;
		$this->specialorder  = (isset($data['specialorder'])) ? $data['specialorder'] : null;
	}

	public function mergeArray($data)
	{
		foreach ($data as $key => $value)
		{
			$this->{$key} = $value;
		}
	}

	public function getArrayCopy()
	{
		return get_object_vars($this);
	}
}