<?php
namespace Application\User\Model;

class User
{
	public $userid;
	public $username;
	public $password;

	public function exchangeArray($data)
	{
		$this->userid     = (isset($data['userid'])) ? $data['userid'] : null;
		$this->username     = (isset($data['username'])) ? $data['username'] : null;
		$this->password     = (isset($data['password'])) ? $data['password'] : null;

	}

	public function getArrayCopy()
	{
		return get_object_vars($this);
	}
}