<?php
namespace Application\User\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Where;

class UserTable
{
	protected $tableGateway;

	public function __construct(TableGateway $tableGateway)
	{
		$this->tableGateway = $tableGateway;
	}

	public function fetchAll()
	{
		$resultSet = $this->tableGateway->select();
		return $resultSet;
	}


	public function getUser($id)
	{
		$id  = (int) $id;
		$rowset = $this->tableGateway->select(array('userid' => $id));
		$row = $rowset->current();
		if (!$row) {
			throw new \Exception("Could not find row $id");
		}
		return $row;
	}

	public function saveUser(User $user)
	{
		$data = $user->getArrayCopy();

		$data['password'] = md5($data['password']);

		$id = (int)$user->userid;
		if ($id == 0) {
			$this->tableGateway->insert($data);
		} else {
			if ($this->getUser($id)) {
				$this->tableGateway->update($data, array('userid' => $id));
			} else {
				throw new \Exception('Video id does not exist');
			}
		}
	}

	public function deleteUser($id)
	{
		$this->tableGateway->delete(array('userid' => $id));
	}
}
