<?php
namespace Application\Collection\Model;

use Zend\Db\TableGateway\TableGateway;

class CollectionTable
{
	protected $tableGateway;

	public function __construct(TableGateway $tableGateway)
	{
		$this->tableGateway = $tableGateway;
	}

	public function fetchChildrenIds($parentid, $children = array())
	{
		$selectedArray = array('parentid' => $parentid, 'special' => 0);
		$data = $this->tableGateway->select($selectedArray);
		foreach ($data as $row)
		{
			$children[] = $row->collectionid;
			$children = $this->fetchChildrenIds($row->collectionid, $children);
		}
		return $children;
	}

	public function fetchChildren($parentid, $excludeSpecial)
	{
		$selectedArray = array('parentid' => $parentid);
		if ($excludeSpecial)
		{
			$selectedArray['special'] = 0;
		}
		$data = $this->tableGateway->select($selectedArray);

		foreach ($data as $row)
		{
			$row->children = $this->fetchChildren($row->collectionid, $excludeSpecial);
		}
		return $data;
	}

	public function fetchChildrenArray($parentid, $excludeSpecial)
	{
		$selectedArray = array('parentid' => $parentid);
		if ($excludeSpecial)
		{
			$selectedArray['special'] = 0;
		}
		$data = $this->tableGateway->select($selectedArray);
		$arrayData = array();
		foreach ($data as $row)
		{
			$newData = $row->getArrayCopy();
			$newData['children'] = $this->fetchChildrenArray($row->collectionid, $excludeSpecial);
			$arrayData[] = $newData;
		}
		return $arrayData;
	}

	public function fetchChildrenOneLevel($parentid, $level, $excludeSpecial)
	{
		$thisData = array();
		$selectedArray = array('parentid' => $parentid);
		if ($excludeSpecial)
		{
			$selectedArray['special'] = 0;
		}
		$data = $this->tableGateway->select($selectedArray);
		foreach ($data as $row)
		{
			$row->depth = $level;
			$thisData[] = $row;
			$thisData = array_merge($thisData, $this->fetchChildrenOneLevel($row->collectionid, $level+1 , $excludeSpecial));
		}
		return $thisData;
	}

	public function fetchAllArray($excludeSpecial = true)
	{
		return $this->fetchChildrenArray(-1, $excludeSpecial);
	}

	public function fetchAll($excludeSpecial = true)
	{
		return $this->fetchChildren(-1, $excludeSpecial);
	}

	public function fetchAllWithoutDepth($excludeSpecial = true)
	{
		return $this->fetchChildrenOneLevel(-1, 0, $excludeSpecial);
	}

	public function fetchSpecial()
	{
		return $this->tableGateway->select(array('special' => 1));
	}

	public function getCollection($id)
	{
		$id  = (int) $id;
		$rowset = $this->tableGateway->select(array('collectionid' => $id));
		$row = $rowset->current();
		if (!$row) {
			throw new \Exception("Could not find row $id");
		}
		return $row;
	}

	private function createUrl($string, $force_lowercase = true, $anal = false) {
		$strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]",
			"}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
			"â€”", "â€“", ",", "<", ".", ">", "/", "?");
		$clean = trim(str_replace($strip, "", strip_tags($string)));
		$clean = preg_replace('/\s+/', "-", $clean);
		$clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean ;
		return ($force_lowercase) ?
			(function_exists('mb_strtolower')) ?
			mb_strtolower($clean, 'UTF-8') :
			strtolower($clean) :
			$clean;
	}

	public function updateVideoCount($id, $count)
	{
		$this->tableGateway->update(array('count' => $count), array('collectionid' => $id));
	}

	public function saveCollection(Collection $collection)
	{
		$data = $collection->getArrayCopy();
		$data['url'] = $this->createUrl($data['title']);
		$id = (int)$collection->collectionid;
		if ($id == 0) {
			$this->tableGateway->insert($data);
		} else {
			if ($this->getCollection($id)) {
				$this->tableGateway->update($data, array('collectionid' => $id));
			} else {
				throw new \Exception('Collection id does not exist');
			}
		}
	}

	public function deleteCollection($id)
	{
		$this->tableGateway->delete(array('collectionid' => $id));
	}
}
