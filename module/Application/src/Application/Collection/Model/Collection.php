<?php
namespace Application\Collection\Model;

class Collection
{
	public $collectionid;
	public $title;
	public $url;
	public $parentid;
	public $children;
	public $depth;
	public $count;
	public $special;

	public function exchangeArray($data)
	{
		$this->collectionid     = (isset($data['collectionid'])) ? $data['collectionid'] : null;
		$this->title     = (isset($data['title'])) ? $data['title'] : null;
		$this->url     = (isset($data['url'])) ? $data['url'] : null;
		$this->parentid = (isset($data['parentid'])) ? $data['parentid'] : null;
		$this->count = (isset($data['count'])) ? $data['count'] : 0;
		$this->special = (isset($data['special'])) ? $data['special'] : 0;
	}

	public function getArrayCopy()
	{
	        $vars = get_object_vars($this);
		unset($vars['children'], $vars['depth']);
		return $vars;
	}
}
