<?php

namespace Application\Collection\Form;

use Zend\Form\Form;
use Zend\InputFilter\Factory as InputFactory;     // <-- Add this import
use Zend\InputFilter\InputFilter;                 // <-- Add this import
use Zend\InputFilter\InputFilterAwareInterface;   // <-- Add this import
use Zend\InputFilter\InputFilterInterface;        // <-- Add this import

class CollectionForm extends Form
{
	private $collectionData;
	protected $inputFilter;                       // <-- Add this variable

	public function __construct($collectionData, $currentID)
	{
		// we want to ignore the name passed
		parent::__construct('collection');
		$this->collectionData = $collectionData;

		$this->setAttribute('method', 'post');
		$this->add(array(
			'name' => 'collectionid',
			'attributes' => array(
				'type'  => 'hidden',
			),
		));

		$this->add(array(
			'name' => 'title',
			'attributes' => array(
				'type'  => 'text',
			),
			'options' => array(
				'label' => 'Title',
			),
		));

		$options = array(-1 => 'None');

		foreach ($this->collectionData as $row)
		{
			if ($row->collectionid == $currentID) continue;
			$options["{$row->collectionid}"] = str_repeat(' -- ', $row->depth) . $row->title;
		}


		$this->add(array(
			'type' => 'Zend\Form\Element\Select', 
			'name' => 'parentid',
			'options' => array(
				'label' => 'Parent',
				'value_options' => $options
			),
		));

		$this->add(array(
			'name' => 'submit',
			'attributes' => array(
				'type'  => 'submit',
				'value' => 'Go',
				'id' => 'submitbutton',
			),
		));
	}

	public function getInputFilter()
	{
		if (!$this->inputFilter) {
			$inputFilter = new InputFilter();

			$inputFilter->add(array(
				'name'     => 'collectionid',
				'required' => true,
				'filters'  => array(
					array('name' => 'Int'),
				),
			));

			$inputFilter->add(array(
				'name'     => 'title',
				'required' => true,
				'filters'  => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
					array(
						'name'    => 'StringLength',
						'options' => array(
							'encoding' => 'UTF-8',
							'min'      => 1,
							'max'      => 100,
						),
					),
				),
			));

			$inputFilter->add(array(
				'name' => 'parentid',
				'required' => true,
				'filters'  => array(
					array('name' => 'Int'),
				),
			));

			$this->inputFilter = $inputFilter;
		}

		return $this->inputFilter;
	}

}
