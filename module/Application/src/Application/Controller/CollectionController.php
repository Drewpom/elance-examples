<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Application\Video\Model\Video;
use Application\Video\Model\VideoTable;

use Application\Collection\Model\Collection;
use Application\Collection\Model\CollectionTable;


class CollectionController extends AbstractActionController
{
	protected $videoTable;
	protected $collectionTable;
	protected $cache;

	public function getVideoTable()
	{
		if (!$this->videoTable) {
			$sm = $this->getServiceLocator();
			$this->videoTable = $sm->get('VideoTable');
		}
		return $this->videoTable;
	}

	public function getCollectionTable()
	{
		if (!$this->collectionTable) {
			$sm = $this->getServiceLocator();
			$this->collectionTable = $sm->get('CollectionTable');
		}
		return $this->collectionTable;
	}

	public function getCacheService()
	{
		if (!$this->cache)
		{
			$this->cache = $this->getServiceLocator()->get('cache');
		}
		return $this->cache;
	}

	public function searchAction()
	{
		$query = $this->params()->fromQuery('search', false);   // From POST
		$result =  $this->getVideoTable()->search($query);

		$this->layout()->setVariable('breadcrumbs', array(
			'Search' => array(
				'routename' => 'search',
				'options' => array()
			))
		);

		$view = new ViewModel(array('title' => "Search Results For '" . htmlentities($query) . "'", 'videos' => $result));
		$view->setTemplate('application/collection/list.phtml'); // path to phtml file under view folder
		return $view;
	}

	public function listAction()
	{
		$collectionid = $this->params()->fromRoute('id', false);   // From POST
		$collection = $this->getCacheService()->getItem('collection_meta_' . $collectionid);
		$url = $this->params()->fromRoute('url', false);   // From POST
		
		$this->layout()->setVariable('breadcrumbs', array(
			$collection->title => array(
				'routename' => 'collection',
				'options' => array('id' => $collection->collectionid, 'url' => $collection->url)
			))
		);
		return new ViewModel(array(
			'title' => $collection->title,
			'videos' => $this->getCacheService()->getItem('collection_data_' . $collectionid),
			'collection' => $collection,
		));
	}
}
