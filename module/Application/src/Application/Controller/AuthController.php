<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Application\Auth\Model\LoginForm;

class AuthController extends AbstractActionController
{
	public $authService;

	public function getAuthService()
	{
		if (!$this->authService)
		{
			$this->authService = $this->getServiceLocator()->get('AuthService');
		}
		return $this->authService;
	}

	public function loginAction()
	{
		$this->layout()->setVariable('breadcrumbs', array(
			'Login' => array(
				'routename' => 'auth'
			))
		);

		if ($this->getAuthService()->hasIdentity())
		{
			return $this->redirect()->toRoute('home');
		}
		$form = new LoginForm();
		$request = $this->getRequest();
		$redirect = 'auth';
		if ($request->isPost()) {
			$form->setInputFilter($form->getInputFilter());
			$form->setData($request->getPost());
			if ($form->isValid()) {
				$this->getAuthService()->getAdapter()
					->setIdentity($request->getPost('username'))
					->setCredential($request->getPost('password'));
				$result = $this->getAuthService()->authenticate();
				foreach ($result->getMessages() as $message)
				{
					$this->flashMessenger()->addMessage($message);
				}
				if ($result->isValid())
				{
					$redirect = 'home';
				}
				return $this->redirect()->toRoute($redirect);
			}
		}

		return new ViewModel(array('form' => $form, 'messages' => $this->flashMessenger()->getMessages()));
	}

	public function successAction()
	{
		return new ViewModel();
	}
}
