<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Application\Video\Model\Video;
use Application\Video\Model\VideoTable;

use Application\Collection\Model\Collection;
use Application\Collection\Model\CollectionTable;


class VideoController extends AbstractActionController
{
	protected $videoTable;
	protected $collectionTable;

	public function getVideoTable()
	{
		if (!$this->videoTable) {
			$sm = $this->getServiceLocator();
			$this->videoTable = $sm->get('VideoTable');
		}
		return $this->videoTable;
	}

	public function getCollectionTable()
	{
		if (!$this->collectionTable) {
			$sm = $this->getServiceLocator();
			$this->collectionTable = $sm->get('CollectionTable');
		}
		return $this->collectionTable;
	}

	public function displayAction()
	{
		$videoid = $this->params()->fromRoute('id', false);   // From POST
		$video = $this->getVideoTable()->getVideo($videoid);
		$collection = $this->getCollectionTable()->getCollection($video->collectionid);
		$url = $this->params()->fromRoute('url', false);   // From POST

		$this->layout()->setVariable('breadcrumbs', 
			array(
				$collection->title => array(
					'routename' => 'collection',
					'options' => array('id' => $collection->collectionid, 'url' => $collection->url)
				),
				$video->title => array(
					'routename' => 'video',
					'options' => array('id' => $video->videoid, 'url' => $video->url)
				)
			)
		);

		$yt = new \Zend_Gdata_YouTube();

		$youtubeComments = $yt->getVideoCommentFeed($video->youtubeid);
		$youtubeComments->setTotalResults(20);

		$url = $this->url()->fromRoute('video', array('action' => 'display', 'id' => $videoid, 'url' => $video->url), array('force_canonical' => true));
		$headCode = '
			<meta property="og:type" content=""/>
			<meta property="og:url" content="' . $url . '"/>
			<meta property="og:title"       content="'.$video->title.'" /> 
			<meta property="og:description" content="'. $video->description.'" /> 
			<meta property="og:image"       content="http://ogp.me/logo.png" />';

		$this->layout()->setVariable('headCode', $headCode);

		$this->getVideoTable()->increaseViewCount($video->videoid);
	
		return new ViewModel(array(
			'video' => $video,
			'collection' => $collection,
			'comments' => $youtubeComments
		));
	}
}
