<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Application\Video\Model\Event;
use Application\Video\Model\EventTable;

class EventController extends AbstractActionController
{
	protected $eventTable;

	public function getEventTable()
	{
		if (!$this->eventTable) {
			$sm = $this->getServiceLocator();
			$this->eventTable = $sm->get('EventTable');
		}
		return $this->eventTable;
	}

	public function displayAction()
	{
		$eventid = $this->params()->fromRoute('id', false);   // From POST
		$event = $this->getEventTable()->getEvent($eventid);
		
		$this->layout()->setVariable('breadcrumbs', array(
			$event->title => array(
				'routename' => 'event',
				'options' => array('id' => $event->eventid, 'url' => $event->url)
			))
		);

		return new ViewModel(array('event' => $event));
	}
}
