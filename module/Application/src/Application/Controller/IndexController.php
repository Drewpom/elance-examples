<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Application\Poll\Model\PollResponse;
use Application\Poll\Model\PollResponseTable;

use Zend\Session\Container;

class IndexController extends AbstractActionController
{
	public $cache;
	public $responseTable;
	public $polloptionTable;

	public function getCacheService()
	{
		if (!$this->cache)
		{
			$this->cache = $this->getServiceLocator()->get('cache');
		}
		return $this->cache;
	}

	public function getPollOptionTable()
	{
		if (!$this->polloptionTable) {
			$sm = $this->getServiceLocator();
			$this->polloptionTable = $sm->get('PollOptionTable');
		}
		return $this->polloptionTable;
	}

	public function getResponseTable()
	{
		if (!$this->responseTable) {
			$sm = $this->getServiceLocator();
			$this->responseTable = $sm->get('PollResponseTable');
		}
		return $this->responseTable;
	}

	public function submitpollAction()
	{
		$container = new Container('myapp');
		if (!$container->hasVoted)
		{
			$answerid = $this->params()->fromPost('poll', false);   // From POST

			$poll = $this->getServiceLocator()->get('cache')->getItem('poll');
			$response = new PollResponse();
			$response->exchangeArray(array('pollid' => $poll['pollid'], 'answerid' => $answerid));
			$this->getResponseTable()->saveResponse($response);

			$pollOption = $this->getPollOptionTable()->getOption($answerid);
			$pollOption->votes++;
			$this->getPollOptionTable()->saveOption($pollOption);

			$cache = $this->getPollOptionTable()->createDataCache(
				$this->getServiceLocator()->get('PollTable')->getPoll($poll['pollid'])
			);

			$this->getServiceLocator()->get('cache')->removeItem('polldata');
			$this->getServiceLocator()->get('cache')->addItem('polldata', $cache);

			$container->hasVoted = 1;
			return $this->redirect()->toRoute('home');
		} else {
			return $this->redirect()->toRoute('home');
		}
	}

	public function indexAction()
	{
		$this->layout()->setVariable('breadcrumbs', array());

		$featuredVideos = $this->getCacheService()->getItem('special_collection_featured');
		$latestVideos = $this->getCacheService()->getItem('special_collection_latest');
		$mostPopular = $this->getCacheService()->getItem('special_collection_popular');

		return new ViewModel(array('featured' => $featuredVideos, 'latest' => $latestVideos, 'mostpopular' => $mostPopular));
	}

}
