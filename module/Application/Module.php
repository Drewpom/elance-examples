<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

use Zend\Authentication\AuthenticationService;
use Zend\Db\Adapter\Adapter as DbAdapter;
use Zend\Authentication\Adapter\DbTable as AuthAdapter;

use Application\Video\Model\Video;
use Application\Video\Model\VideoTable;
use Application\Collection\Model\Collection;
use Application\Collection\Model\CollectionTable;
use Application\Event\Model\Event;
use Application\Event\Model\EventTable;
use Application\Poll\Model\PollResponse;
use Application\Poll\Model\PollResponseTable;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;


class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

	public function getViewHelperConfig()
	{
		return array(
			'factories' => array(
				'getCollections' => function($sm) {
					$serviceLocator = $sm->getServiceLocator();
					$viewHelper = new \Application\Helpers\GetCollections($serviceLocator);
					return $viewHelper;
				},
				'getEvents' => function($sm) {
					$serviceLocator = $sm->getServiceLocator();
					$viewHelper = new \Application\Helpers\GetEvents($serviceLocator);
					return $viewHelper;
				},
				'getPoll' => function($sm) {
					$serviceLocator = $sm->getServiceLocator();
					$viewHelper = new \Application\Helpers\GetPoll($serviceLocator);
					return $viewHelper;
				},
				'drawCalendar' => function($sm) {
					$serviceLocator = $sm->getServiceLocator();
					$viewHelper = new \Application\Helpers\DrawCalendar($serviceLocator);
					return $viewHelper;
				}
			)
		);
	}

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'VideoTable' =>  function($sm) {
                    $tableGateway = $sm->get('VideoTableGateway');
                    $table = new \Application\Video\Model\VideoTable($tableGateway);
                    return $table;
                },
                'VideoTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new \Application\Video\Model\Video());
                    return new TableGateway('video', $dbAdapter, null, $resultSetPrototype);
                },
                'CollectionTable' =>  function($sm) {
                    $tableGateway = $sm->get('CollectionTableGateway');
                    $table = new \Application\Collection\Model\CollectionTable($tableGateway);
                    return $table;
                },
                'CollectionTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new \Application\Collection\Model\Collection());
                    return new TableGateway('collection', $dbAdapter, null, $resultSetPrototype);
                },
                'EventTable' =>  function($sm) {
                    $tableGateway = $sm->get('EventTableGateway');
                    $table = new \Application\Event\Model\EventTable($tableGateway);
                    return $table;
                },
                'EventTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new \Application\Event\Model\Event());
                    return new TableGateway('event', $dbAdapter, null, $resultSetPrototype);
                },
                'PollResponseTable' =>  function($sm) {
                    $tableGateway = $sm->get('PollResponseTableGateway');
                    $table = new \Application\Poll\Model\PollResponseTable($tableGateway);
                    return $table;
                },
                'PollResponseTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new \Application\Poll\Model\PollResponse());
                    return new TableGateway('poll_response', $dbAdapter, null, $resultSetPrototype);
                },
            ),
        );
    }
}
