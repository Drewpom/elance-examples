<?php
error_reporting(E_ALL & ~E_NOTICE);

// #################### DEFINE IMPORTANT CONSTANTS #######################
define('THIS_SCRIPT', 'wiki');
define('CSRF_PROTECTION', true);

// ################### PRE-CACHE TEMPLATES AND DATA ######################
// get special phrase groups
$phrasegroups = array();

// get special data templates from the datastore
$specialtemplates = array();

// pre-cache templates used by all actions
$globaltemplates = array();

// pre-cache templates used by specific actions
$actiontemplates = array();

// ######################### REQUIRE BACK-END ############################
require_once('./global.php');
require_once(DIR . '/includes/class_reportitem.php');
require_once(DIR . '/includes/class_reportwiki.php');


$canEdit = ($permissions['wikipermissions'] & $vbulletin->bf_ugp_wikipermissions['canedit']);


if ($_REQUEST['do'] == 'makewiki')
{
	$vbulletin->input->clean_array_gpc('r', array(
		'threadid' => TYPE_UINT
	));

	if (!($permissions['wikipermissions'] & $vbulletin->bf_ugp_wikipermissions['canmakewiki']))
	{
		print_no_permission();
	}

	$thread = fetch_threadinfo($vbulletin->GPC['threadid']);

	$threadman =& datamanager_init('Thread', $vbulletin, ERRTYPE_STANDARD, 'threadpost');
	$threadman->set_existing($thread);
	$threadman->set('wikienabled', 1);
	$threadman->pre_save();
	$threadman->save();

	$url = $vbulletin->options['bburl'] . '/showthread.php?t=' . $vbulletin->GPC['threadid'];
	exec_header_redirect($url);
}
if ($_REQUEST['do'] == 'editwiki')
{
	$vbulletin->input->clean_array_gpc('r', array(
		'threadid' => TYPE_UINT,
		'message' => TYPE_STR,
		'wysiwyg' => TYPE_BOOL
	));

	if (!$canEdit)
	{
		print_no_permission();
	}

	$current = $db->query_first("SELECT * FROM " . TABLE_PREFIX . "wikithreads WHERE threadid = '" . $vbulletin->GPC['threadid'] . "'");
	if (!$current)
	{
		$db->query_write("INSERT INTO " . TABLE_PREFIX . "wikithreads (threadid, wiki, unformatted) VALUES ('" . $vbulletin->GPC['threadid'] . "', '', '')");
		$current = array('unformatted' => '');
	}

	if ($current['locked'])
	{
		print_no_permission();
	}

	$lastedit = $db->query_first("SELECT dateline FROM " . TABLE_PREFIX . "wikiedits WHERE userid = '" . $vbulletin->userinfo['userid'] . "'");
	$editwaittime = TIMENOW - 600;
	if ($lastedit['dateline'] > $editwaittime)
	{
		standard_error(construct_phrase($vbphrase['wiki_edit_too_soon'], '10 Minutes'));
	}

	$wikitext = $vbulletin->GPC['message'];
	if ($vbulletin->GPC['wysiwyg'])
	{
		require_once DIR . '/includes/class_wysiwygparser.php';
		$html_parser = new vB_WysiwygHtmlParser(vB::$vbulletin);
		$wikitext = $html_parser->parse_wysiwyg_html_to_bbcode($wikitext);
	}

	//TRACK EDITS
	if ($wikitext != $current['unformatted'])
	{
		$db->query_write("INSERT INTO " . TABLE_PREFIX . "wikiedits (userid, dateline, threadid, beforetext, aftertext) VALUES (
			'" . $vbulletin->userinfo['userid'] . "',
			'" . TIMENOW . "',
			'" . $vbulletin->GPC['threadid'] . "',
			'" . $db->escape_string($current['unformatted']) . "',
			'" . $db->escape_string($wikitext) . "')");
		$lasteditid = $db->insert_id();
	} else {
		$record = $db->query_first("SELECT MAX(id) as id FROM " . TABLE_PREFIX . "wikiedits WHERE threadid = '" . $vbulletin->GPC['threadid'] . "'");
		$lasteditid = $record['id'];
	}

	require_once(DIR . '/includes/class_bbcode.php'); 
	$parser = new vB_BbCodeParser($vbulletin, fetch_tag_list());
	$formatted = $parser->do_parse($wikitext);
	
	$db->query_write("UPDATE " . TABLE_PREFIX . "wikithreads SET
		wiki = '" . $db->escape_string($formatted) . "',
		unformatted = '" . $db->escape_string($wikitext) . "',
		lasteditid = '" . $lasteditid . "'
		WHERE threadid = '" . $vbulletin->GPC['threadid'] . "'");

	$url = $vbulletin->options['bburl'] . '/showthread.php?t=' . $vbulletin->GPC['threadid'];
	exec_header_redirect($url);

}
if ($_REQUEST['do'] == 'restoreentry')
{
	$vbulletin->input->clean_array_gpc('r', array(
		'editid' => TYPE_UINT
	));

	if (!($permissions['wikipermissions'] & $vbulletin->bf_ugp_wikipermissions['canmoderate']))
	{
		print_no_permission();
	}

	$edit = $db->query_first("SELECT * FROM " . TABLE_PREFIX . "wikiedits WHERE id = '" . $vbulletin->GPC['editid'] . "'");

	$neweredits = $db->query_first("SELECT * FROM " . TABLE_PREFIX . "wikiedits WHERE threadid = '" . $edit['threadid'] . "' AND dateline > '" . $edit['dateline'] . "'");
	if (!$neweredits)
	{
		//Get the next newest edit...
		require_once(DIR . '/includes/class_bbcode.php'); 
		$parser = new vB_BbCodeParser($vbulletin, fetch_tag_list());
		$edit['aftertextformatted'] = $parser->do_parse($edit['aftertext']);
		$db->query_write("UPDATE " . TABLE_PREFIX . "wikithreads SET
			wiki = '" . $db->escape_string($edit['aftertextformatted']) . "',
			unformatted = '" . $db->escape_string($edit['aftertext']) . "',
			lasteditid = '" . $edit['id'] . "'
			WHERE threadid = '" . $edit['threadid'] . "'");
	}

	$db->query_write("UPDATE " . TABLE_PREFIX . "wikiedits SET deleted = '0' WHERE id = '" . $edit['id'] . "'");

	$url = $vbulletin->options['bburl'] . '/wiki.php?do=viewwikihistory&threadid=' . $edit['threadid'];
	exec_header_redirect($url);
}

if ($_REQUEST['do'] == 'deleteentry')
{
	$vbulletin->input->clean_array_gpc('r', array(
		'editid' => TYPE_UINT
	));

	if (!($permissions['wikipermissions'] & $vbulletin->bf_ugp_wikipermissions['canmoderate']))
	{
		print_no_permission();
	}

	$edit = $db->query_first("SELECT * FROM " . TABLE_PREFIX . "wikiedits WHERE id = '" . $vbulletin->GPC['editid'] . "'");
	if (empty($edit['beforetext']))
	{
		standard_error($vbphrase['wiki_cannot_delete_first']);
	}

	$wiki = $db->query_first("SELECT * FROM " . TABLE_PREFIX . "wikithreads WHERE threadid = '" . $edit['threadid'] . "'");
	if ($wiki['lasteditid'] == $edit['id'])
	{
		//Get the next newest edit...
		$replace = $db->query_first("SELECT * FROM " . TABLE_PREFIX . "wikiedits WHERE threadid = '" . $edit['threadid'] . "' AND id != '" . $edit['id'] . "' ORDER BY dateline DESC");
		require_once(DIR . '/includes/class_bbcode.php'); 
		$parser = new vB_BbCodeParser($vbulletin, fetch_tag_list());
		$replace['aftertextformatted'] = $parser->do_parse($replace['aftertext']);
		$db->query_write("UPDATE " . TABLE_PREFIX . "wikithreads SET
			wiki = '" . $db->escape_string($replace['aftertextformatted']) . "',
			unformatted = '" . $db->escape_string($replace['aftertext']) . "',
			lasteditid = '" . $replace['id'] . "'
			WHERE threadid = '" . $replace['threadid'] . "'");
	}

	$db->query_write("UPDATE " . TABLE_PREFIX . "wikiedits SET deleted = '1' WHERE id = '" . $edit['id'] . "'");

	$url = $vbulletin->options['bburl'] . '/wiki.php?do=viewwikihistory&threadid=' . $edit['threadid'];
	exec_header_redirect($url);
}
if ($_REQUEST['do'] == 'revertto')
{
	$vbulletin->input->clean_array_gpc('r', array(
		'editid' => TYPE_UINT
	));

	if (!($permissions['wikipermissions'] & $vbulletin->bf_ugp_wikipermissions['canmoderate']))
	{
		print_no_permission();
	}

	$edit = $db->query_first("SELECT * FROM " . TABLE_PREFIX . "wikiedits WHERE id = '" . $vbulletin->GPC['editid'] . "'");

	require_once(DIR . '/includes/class_bbcode.php'); 
	$parser = new vB_BbCodeParser($vbulletin, fetch_tag_list());
	$edit['aftertextformatted'] = $parser->do_parse($edit['aftertext']);
	$db->query_write("UPDATE " . TABLE_PREFIX . "wikithreads SET
		wiki = '" . $db->escape_string($edit['aftertextformatted']) . "',
		unformatted = '" . $db->escape_string($edit['aftertext']) . "',
		lasteditid = '" . $edit['id'] . "'
		WHERE threadid = '" . $edit['threadid'] . "'");

	//"Soft" delete all newer edits.
	$db->query_write("UPDATE " . TABLE_PREFIX . "wikiedits SET deleted = '1' WHERE threadid = '" . $edit['threadid'] . "' AND dateline > '" . $edit['dateline'] . "'");

	$url = $vbulletin->options['bburl'] . '/wiki.php?do=viewwikihistory&threadid=' . $edit['threadid'];
	exec_header_redirect($url);

}
if ($_REQUEST['do'] == 'viewwikihistory')
{
	$vbulletin->input->clean_array_gpc('r', array(
		'threadid' => TYPE_UINT,
		'page' => TYPE_UINT,
		'perpage' => TYPE_UINT,
		'unformatted' => TYPE_BOOL
	));

	require_once(DIR . '/includes/class_diff.php');

	if (!($permissions['wikipermissions'] & $vbulletin->bf_ugp_wikipermissions['canviewhistory']))
	{
		print_no_permission();
	}

	$where = (is_member_of($vbulletin->userinfo, 5, 6, 7)) ? '' : ' AND deleted = 0';
	if ($vbulletin->GPC_exists['threadid'] AND $vbulletin->GPC['threadid'] != 0)
	{
		$threadinfo = fetch_threadinfo($vbulletin->GPC['threadid']);
		$title = construct_phrase($vbphrase['wiki_edit_history'], $threadinfo['title']);
		$threadlink = fetch_seo_url('thread', $threadinfo);
		$navbits[$threadlink] = $threadinfo['title'];
		$where .= " AND threadid = '" . $vbulletin->GPC['threadid'] . "'";
	} else if (is_member_of($vbulletin->userinfo, 5, 6, 7)) {
		$title = construct_phrase($vbphrase['wiki_recent_edits']);
		$join = "LEFT JOIN " . TABLE_PREFIX . "thread as thread ON(thread.threadid = wikiedits.threadid)";
		$fields = ', thread.title as title'; 
	} else {
		print_no_permission();
	}
	$navbits[''] = $title;
	$navbits = construct_navbits($navbits);
	$navbar = render_navbar_template($navbits);


	$counter = $db->query_first("SELECT COUNT(id) as total FROM " . TABLE_PREFIX . "wikiedits WHERE 1=1 $where");

	$pagenumber = ($vbulletin->GPC['page']) ? $vbulletin->GPC['page'] : 1;
	$perpage = ($vbulletin->GPC['perpage']) ? $vbulletin->GPC['perpage'] : 5;
	sanitize_pageresults($counter['total'], $pagenumber, $perpage, 20, 5); 
	if ($vbulletin->GPC['page'] < 1) {
		$vbulletin->GPC['page'] = 1;
	} else if ($vbulletin->GPC['page'] > ceil(($counter['total'] + 1) / $perpage)) {
		$vbulletin->GPC['page'] = ceil(($counter['total'] + 1) / $perpage);
	}
	$limitlower = ($vbulletin->GPC['page'] - 1) * $perpage;
	$limitupper = ($vbulletin->GPC['page']) * $perpage;  
	$pagenav = construct_page_nav(
		$vbulletin->GPC['page'],
		$perpage,
		$counter['total'],
		'wiki.php?' . $vbulletin->session->vars['sessionurl'], // the pagenav-link
		'&amp;do=viewwikihistory&amp;threadid=' . $vbulletin->GPC['threadid'] .'&amp;perpage=' . $perpage
	);


	$edits = $db->query_read("SELECT wikiedits.*, user.username $fields FROM " . TABLE_PREFIX . "wikiedits as wikiedits
		LEFT JOIN " . TABLE_PREFIX . "user as user ON(user.userid = wikiedits.userid)
		$join
		WHERE 1=1 $where ORDER BY dateline DESC LIMIT $limitlower, $perpage");
	$data = array(); 
	$wrap = true;
	require_once(DIR . '/includes/class_bbcode.php'); 
	require_once(DIR . '/includes/class_diff.php');
	$parser = new vB_BbCodeParser($vbulletin, fetch_tag_list());
 
	while ($row = $db->fetch_array($edits))
	{
		$row['isfirst'] = (empty($row['beforetext'])); 
		$diff = new vB_Text_Diff($row['beforetext'], $row['aftertext']);
		$entries =& $diff->fetch_diff();

		$row['text'] = '';
		foreach ($entries AS $diff_entry)
		{
			foreach ($diff_entry->fetch_data_new() as $line)
			{
				$class = $diff_entry->fetch_data_new_class();

				if ($vbulletin->GPC['unformatted'])
				{
					$row['text'] .= '<div class="' .$class.'">'. $line . '</div>';
					continue;
				}

				if (!($diff_entry->fetch_data_new_class() == 'changed' OR $diff_entry->fetch_data_new_class() == 'added'))
				{
					$row['text'] .= $line;
					continue;
				}

				$handled = false;

				//Handle Lists
				$length = substr_count($line, '[*]');
				$previousPosition = 0;
				for ($i = 0; $i < $length; $i++)
				{
					$startTag = '[*]';
					$endTag = '[*]';

					$elementStart = stripos($line, $startTag, $previousPosition)+strlen($startTag);
					if ($elementStart === false)
					{
						$elementEnd = 0;
					}
					$previousPosition = $elementStart;

					$elementEnd = stripos($line, $endTag, $previousPosition);
					if ($elementEnd === false)
					{
						$elementEnd = strlen($line);
					} else {
						$elementEnd = $elementEnd - strlen($endTag) + 1;
					}

					$content = substr($line, $elementStart, $elementEnd);
					$row['text'] .= $startTag . '<div class="' . $class. '">' . $content . '</div>';
					$handled = true;
				}

				//Handle Tds
				$length = substr_count($line, '[td]')+substr_count($line, '[TD]');
				$previousPosition = 0;
				for ($i = 0; $i < $length; $i++)
				{
					$startTag = '[td]';
					$endTag = '[/td]';

					$elementStart = stripos($line, $startTag, $previousPosition)+strlen($startTag);
					if ($elementStart === false)
					{
						$elementEnd = 0;
					}
					$previousPosition = $elementStart;

					$elementEnd = stripos($line, $endTag, $previousPosition);
					if ($elementEnd === false)
					{
						$elementEnd = strlen($line);
					} else {
						$elementEnd = $elementEnd - strlen($endTag) + 1;
					}

					$content = substr($line, $elementStart, $elementEnd);
					$row['text'] .= $startTag . '<div class="' . $class. '">' . $content . '</div>' . $endTag;
					$handled = true;
				}

				if (!$handled)
				{
					$line = (empty($line)) ? '&nbsp;' : $line;
					$row['text'] .= '<div class="' . $class. '">' . $line . '</div>';
				}
			}
		}

		if (!$vbulletin->GPC['unformatted'])
		{
			$row['text'] = $parser->do_parse($row['text'], 1);
		}
		
		$data[] = $row;
	}


	$show['revertlink'] = ($permissions['wikipermissions'] & $vbulletin->bf_ugp_wikipermissions['canmoderate']);

	$tmp = vB_Template::create('wiki_edithistory'); 
	$tmp->register('history', $data);
	$tmp->register('thread', $threadinfo);
	$tmp->register('pagenav', $pagenav);
	$tmp->register('pagenumber', $pagenumber);
	$tmp->register('tableheader', $title); 
	$tmp->register('unformatted', $vbulletin->GPC['unformatted']); 

	$HTML = $tmp->render();


	$templater = vB_Template::create('GENERIC_SHELL');
	$templater->register_page_templates();
	$templater->register('HTML', $HTML);
	$templater->register('pagetitle', $title);
	$templater->register('navbar', $navbar);

	print_output($templater->render());

}
if ($_REQUEST['do'] == 'report' OR $_POST['do'] == 'sendemail')
{
	$reportthread = ($rpforumid = $vbulletin->options['rpforumid'] AND $rpforuminfo = fetch_foruminfo($rpforumid));
	$reportemail = ($vbulletin->options['enableemail'] AND $vbulletin->options['rpemail']);

	if (!$reportthread AND !$reportemail)
	{
		eval(standard_error(fetch_error('emaildisabled')));
	}

	$vbulletin->input->clean_gpc('r', 'threadid', TYPE_UINT);

	$wiki = $db->query_first_slave("
		SELECT
			*
		FROM " . TABLE_PREFIX . "wikithreads
		WHERE threadid =" . $vbulletin->GPC['threadid'] . "
	");

	if (!$wiki)
	{
		eval(standard_error(fetch_error('invalidid', $vbphrase['thread'], $vbulletin->options['contactuslink'])));
	}

	$reportobj = new vB_ReportItem_Wiki($vbulletin);
	$reportobj->set_extrainfo('wiki', $wiki);
	$perform_floodcheck = $reportobj->need_floodcheck();

	if ($perform_floodcheck)
	{
		$reportobj->perform_floodcheck_precommit();
	}

	($hook = vBulletinHook::fetch_hook('report_start')) ? eval($hook) : false;

	if ($_REQUEST['do'] == 'report')
	{
		// draw nav bar
		$navbits = array(
			'' => $vbphrase['report_bad_wiki']
		);

		$usernamecode = vB_Template::create('newpost_usernamecode')->render();

		$navbits = construct_navbits($navbits);
		$navbar = render_navbar_template($navbits);
		$url =& $vbulletin->url;

		$wiki['itemlink'] = 'showthread.php?' . $vbulletin->session->vars['sessionurl_q'] . "&amp;t=" . $wiki['threadid'];

		($hook = vBulletinHook::fetch_hook('report_form_start')) ? eval($hook) : false;

		$forminfo = $reportobj->set_forminfo($wiki);
		$templater = vB_Template::create('reportitem');
			$templater->register_page_templates();
			$templater->register('forminfo', $forminfo);
			$templater->register('navbar', $navbar);
			$templater->register('url', $url);
			$templater->register('usernamecode', $usernamecode);
		print_output($templater->render());
	}

	if ($_POST['do'] == 'sendemail')
	{
		$vbulletin->input->clean_array_gpc('p', array(
			'reason' => TYPE_STR,
		));

		if ($vbulletin->GPC['reason'] == '')
		{
			eval(standard_error(fetch_error('noreason')));
		}

		$reportobj->do_report($vbulletin->GPC['reason'], $wiki);

		$url =& $vbulletin->url;
		print_standard_redirect('redirect_reportthanks');  
	}
}
if ($_REQUEST['do'] == 'delete')
{
	$vbulletin->input->clean_array_gpc('r', array(
		'threadid' => TYPE_UINT
	));

	$canLock = ($permissions['wikipermissions'] & $vbulletin->bf_ugp_wikipermissions['canlock']);
	if (!$canLock)
	{
		print_no_permission();
	}

	$db->query_write("UPDATE " . TABLE_PREFIX . "wikithreads SET
		active = '0'
		WHERE threadid = '" . $vbulletin->GPC['threadid'] . "'");

	$url = $vbulletin->options['bburl'] . '/showthread.php?t=' . $vbulletin->GPC['threadid'];
	exec_header_redirect($url);
}

if ($_REQUEST['do'] == 'lockediting')
{
	$vbulletin->input->clean_array_gpc('r', array(
		'threadid' => TYPE_UINT
	));

	$canLock = ($permissions['wikipermissions'] & $vbulletin->bf_ugp_wikipermissions['canlock']);
	if (!$canLock)
	{
		print_no_permission();
	}

	$status = $db->query_first("SELECT locked FROM " . TABLE_PREFIX . "wikithreads WHERE threadid = '" . $vbulletin->GPC['threadid'] . "'");
	$locked = ($status['locked']) ? '0' : '1';
	$db->query_write("UPDATE " . TABLE_PREFIX . "wikithreads SET
		locked = '" . $locked . "'
		WHERE threadid = '" . $vbulletin->GPC['threadid'] . "'");

	$url = $vbulletin->options['bburl'] . '/showthread.php?t=' . $vbulletin->GPC['threadid'];
	exec_header_redirect($url);
}

?>